/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.uoc.kison.RGO;

import java.io.File;

import interviews.graphs.Edge;

import org.uoc.kison.objects.SimpleIntGraph;

/**
 *
 * @author jcasasr
 */
public class GraphReconstructionByRS extends GraphReconstruction {

    public static final String PARAM = "RS";
    public static final String NAME = "Random Swap";
    public static final int NUM = 2;

    public GraphReconstructionByRS(SimpleIntGraph g, int iterations, int anon, String fullPath, String baseName, String extension) {
        super(g, iterations, anon, fullPath, baseName, extension);

        logger.info(String.format("GraphReconstructionByRS: Using method '%s'", NAME));
    }

    @Override
    protected int oneEdgeModification() {

        // select two edges at random
        boolean done = false;
        while(!done) {
            Edge e1 = this.getRandomEdge();
            Edge e2 = this.getRandomEdge();
            
            if(e1.v != e2.v && e1.v != e2.w && e1.w != e2.v && e1.w != e2.w) {
                if(!this.gk.getEdges(e1.v).contains(e2.v) && !this.gk.getEdges(e1.w).contains(e2.w)) {
                    // (e1.v, e1.w), (e2.v, e2.w) -> (e1.v, e2.v), (e1.w, e2.w)
                    this.deleteEdge(e1.v, e1.w);
                    this.deleteEdge(e2.v, e2.w);
                    this.addEdge(e1.v, e2.v);
                    this.addEdge(e1.w, e2.w);
                    
                    done = true;
                } else if(!this.gk.getEdges(e1.v).contains(e2.w) && !this.gk.getEdges(e1.w).contains(e2.v)) {
                    // (e1.v, e1.w), (e2.v, e2.w) -> (e1.v, e2.w), (e1.w, e2.v)
                    this.deleteEdge(e1.v, e1.w);
                    this.deleteEdge(e2.v, e2.w);
                    this.addEdge(e1.v, e2.w);
                    this.addEdge(e1.w, e2.v);
                    
                    done = true;
                }
            }
        }
        
        return NUM;
    }
    
    @Override
    protected String getFullPath(int i, int a) {
    	File file = new File(fullPath + PARAM + "/");
        file.mkdirs();
        return fullPath + PARAM + "/"+ baseName + "-" + i + "-" + a + "." + extension;
    }
    
    @Override
	void doPreProcess() {		
	}

	@Override
	void doPostProcess() {
	}
}
