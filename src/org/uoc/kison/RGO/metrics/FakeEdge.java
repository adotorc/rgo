package org.uoc.kison.RGO.metrics;

public class FakeEdge {
	
	private int source;
	private int target;
	private double edgeNeighbourhoodCentrality;
	private double probability;

		
	public FakeEdge() {}

	public FakeEdge(int source, int target, double edgeNeighbourhoodCentrality,
			double probability) {
		this.source = source;
		this.target = target;
		this.edgeNeighbourhoodCentrality = edgeNeighbourhoodCentrality;
		this.probability = probability;
	}
	
	public int getSource() {
		return source;
	}
	public void setSource(int source) {
		this.source = source;
	}
	public int getTarget() {
		return target;
	}
	public void setTarget(int target) {
		this.target = target;
	}
	public double getEdgeNeighbourhoodCentrality() {
		return edgeNeighbourhoodCentrality;
	}
	public void setEdgeNeighbourhoodCentrality(double edgeNeighbourhoodCentrality) {
		this.edgeNeighbourhoodCentrality = edgeNeighbourhoodCentrality;
	}
	public double getProbability() {
		return probability;
	}
	public void setProbability(double probability) {
		this.probability = probability;
	}

	
}
