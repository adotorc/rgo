/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.uoc.kison.RGO.metrics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.uoc.kison.RGO.utils.UtilsGraph;
import org.uoc.kison.objects.SimpleIntGraph;

/**
 *
 * @author jcasasr
 */
public class EdgeNeighbourhoodCentrality {
	
	public static final int Equal = 1;
	public static final int OneOverX = 2;
	public static final int OneOverX2 = 3;
	private static final Logger logger = Logger.getLogger(EdgeNeighbourhoodCentrality.class);
    
    public float getEdgeNeighbourhoodCentrality(SimpleIntGraph gk, int i, int j) {
    	return getEdgeNeighbourhoodCentrality(gk, i, j, 2 * gk.getDegreeMax());
    }
    
    public float getEdgeNeighbourhoodCentrality(SimpleIntGraph gk, int i, int j, int maxdeg) {
        Set<Integer> Ni = new HashSet<Integer>(gk.getEdges(i));
        Set<Integer> Nj = new HashSet<Integer>(gk.getEdges(j));
        
        int orSet = getSetCardinalityUnion(Ni, Nj);
        int andSet = getSetCardinalityIntersection(Ni, Nj);
        
        return (orSet - andSet) / (float)maxdeg;
    }
    
    private int getSetCardinalityUnion(Set<Integer> set1, Set<Integer> set2) {
        // initiate 's' with elements of 'set1'
        Set<Integer> s = new HashSet<Integer>(set1);
        // add elements from 'set2'
        for(Integer i : set2) {
            s.add(i);
        }
        return s.size();
    }
    
    /**
     * Intersection = size(A)+size(B)-Union(A,B)
     * @param set1
     * @param set2
     * @return cardinality of the intersection A,B
     */
    private int getSetCardinalityIntersection(Set<Integer> set1, Set<Integer> set2) {
        int total = set1.size() + set2.size();
        int union = getSetCardinalityUnion(set1, set2);
        return total - union;
    }
    
    public ArrayList<Double> computeEdgeNeighbourhoodCentrality(SimpleIntGraph g) {
	    int maxdeg = 2 * g.getDegreeMax();
	    
	    // compute the ENC for each edge
	    ArrayList<Double> values = new ArrayList<Double>(g.getNumEdges());
	    int n = g.getNumNodes();	    
	    for (int i = 0; i < n; i++) {
			ArrayList<Integer> adj = g.getEdges(i);
			for(int j=0; j< adj.size(); j++) values.add((double) getEdgeNeighbourhoodCentrality(g, i, adj.get(j), maxdeg));
		}

	    return values;
	}
    
    
    public ArrayList<Double> getProbabilityDistribution(ArrayList<Double> values, int type) {
    	ArrayList<Double> p = new ArrayList<Double>(values.size());
    	
    	switch (type) {
			case Equal:
				double res = 1.0 / values.size();
				for (int i=0; i < values.size(); i++) p.add(res);
				
				break;
			
			case OneOverX:
				for (int i=0; i < values.size(); i++) p.add(1.0 / values.get(i));
				
				break;
			case OneOverX2:
				for (int i=0; i < values.size(); i++) p.add(1.0 / Math.pow(values.get(i),2));
				
				break;
				
			default:
				System.out.println(String.format("getProbabilityDistribution:: Unknown function '%d'", type));
				break;
		}

		// remove 'Inf' values and normalize
    	double max = Double.NEGATIVE_INFINITY;
    	double sum = 0.0;
    	for (int i = 0; i < p.size(); i++){
    		double value = p.get(i);
    		if (value > max && value != Double.POSITIVE_INFINITY) max = value;
    		sum += value;
    	}
    	
    	for (int i = 0; i < p.size(); i++){
    		double value = p.get(i);
    		if (value == Double.NEGATIVE_INFINITY || value == Double.POSITIVE_INFINITY)	p.set(i, max / sum);
    		else p.set(i, value / sum);
    	}
		  
		return p;
    }
    
    public ArrayList<FakeEdge> computeFakeEdgesSet(SimpleIntGraph g) {
    	logger.debug("Computing fake edges set...");
    	  
    	logger.debug("Computing adjency matrix...");
    	
    	UtilsGraph utilsGraph = new UtilsGraph();    	
    	SimpleIntGraph gFakeEdges = utilsGraph.copyGraph(g);
  
		int numTotal = g.getNumEdges();
		ArrayList<FakeEdge> fakeEdges = new ArrayList<FakeEdge>(numTotal);
		ArrayList<Double> enc = new ArrayList<Double>(numTotal);
		logger.debug(String.format("Evaluating %d fake edges...", numTotal));
		  
		for(int i=0; i<numTotal; i++) {
			FakeEdge fEdge = new FakeEdge();
			// search non-exitent edge
			ArrayList<Integer> e = UtilsGraph.findValueInMatrix(gFakeEdges, false);
			fEdge.setSource(e.get(0));
			fEdge.setTarget(e.get(1));
			// compute its NC score
			// 1) create fake edge
			gFakeEdges.addEdge(e.get(0), e.get(1));
			gFakeEdges.addEdge(e.get(1), e.get(0));
			// 2) compute score 
			int maxdeg = 2 * g.getDegreeMax();
			double encValue = getEdgeNeighbourhoodCentrality(gFakeEdges, e.get(0), e.get(1), maxdeg);
			fEdge.setEdgeNeighbourhoodCentrality(encValue);
			enc.add(encValue);
			
			fakeEdges.add(fEdge);
		}
		// Allows the GC to free the copy of g
		gFakeEdges = null;
	  
		// compute probability distribution
		ArrayList<Double> pDist = getProbabilityDistribution(enc, EdgeNeighbourhoodCentrality.OneOverX2);
		for (int i = 0; i < pDist.size(); i++) fakeEdges.get(i).setProbability(pDist.get(i));

		logger.debug("...done!");
	  
		return fakeEdges;    
    } 
}
