/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.uoc.kison.RGO;

import java.io.File;

import interviews.graphs.Edge;

import org.uoc.kison.objects.SimpleIntGraph;

/**
 *
 * @author jcasasr
 */
public class GraphReconstructionByRP extends GraphReconstruction {

    public static final String PARAM = "RP";
    public static final String NAME = "Random Add/Del";
    public static final int NUM = 1;

    public GraphReconstructionByRP(SimpleIntGraph g, int iterations, int anon, String fullPath, String baseName, String extension) {
        super(g, iterations, anon, fullPath, baseName, extension);

        logger.info(String.format("GraphReconstructionByRP: Using method '%s'", NAME));
    }

    @Override
    protected int oneEdgeModification() {
        
        /**
         * Delete one edge
         */
        // select one edge at random
        Edge e1 = this.getRandomEdge();
        logger.debug(String.format("Deleting edge (%s,%s)", e1.v, e1.w));
        this.deleteEdge(e1.v, e1.w);
        
        /**
         * Add one edge
         */
        boolean done = false;
        while(!done) {
            int v = this.getRandomVertexNumber();
            int w = this.getRandomVertexNumber();
            
            if(v != w) {
                if(!this.gk.getEdges(v).contains(w)) {
                    // add new edge
                    logger.debug(String.format("Adding edge (%s,%s)", v, w));
                    this.addEdge(v, w);
                    
                    done = true;
                }
            }
        }
        
        return NUM;
    }
    
    @Override
    protected String getFullPath(int i, int a) {
    	File file = new File(fullPath + PARAM + "/");
        file.mkdirs();
        return fullPath + PARAM + "/"+ baseName + "-" + i + "-" + a + "." + extension;
    }
    
    @Override
	void doPreProcess() {	
	}

	@Override
	void doPostProcess() {
	}
}
