/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.uoc.kison.RGO;

import interviews.graphs.Graph;

import org.apache.log4j.Logger;
import org.uoc.kison.RGO.utils.ElapsedTime;
import org.uoc.kison.RGO.utils.UtilsGraph;
import org.uoc.kison.objects.SimpleIntGraph;

/////////////////////////////////////////////////////////
// Univariant Microaggregation for Graph Anonymization //
/////////////////////////////////////////////////////////
public class Process {

    private static final Logger logger = Logger.getLogger(Process.class);
    protected UtilsGraph utilsGraph;
    protected String fullPath;
    protected String baseName;
    protected String extension;

    /**
     * 
     * @param fullPath
     * @param baseName
     * @param extension 
     */
    public Process(String fullPath, String baseName, String extension) {
        utilsGraph = new UtilsGraph();
        this.fullPath = fullPath;
        this.baseName = baseName;
        this.extension = extension;
    }

    /**
     * 
     * @param g
     * @param iterations
     * @param anon
     * @param edgeSelection
     * @return 
     */ 
    public void execute(SimpleIntGraph g, int iterations, int anon, String edgeSelection) {

        ElapsedTime et = new ElapsedTime();
        
        logger.info(String.format("Original Graph: %d nodes, %d edges", g.getNumNodes(), g.getNumEdges()));
        
        /**
         * Modify original graph to perturbe it
         */
        GraphReconstruction gr = null;
        
        if (edgeSelection.compareToIgnoreCase(GraphReconstructionByRS.PARAM) == 0) {
            // GraphReconstructionByRS
            gr = new GraphReconstructionByRS(g, iterations, anon, fullPath, baseName, extension);
        } else if (edgeSelection.compareToIgnoreCase(GraphReconstructionByRP.PARAM) == 0){
            // GraphReconstructionByRP
            gr = new GraphReconstructionByRP(g, iterations, anon, fullPath, baseName, extension);
        } else if (edgeSelection.compareToIgnoreCase(GraphReconstructionByNC.PARAM) == 0){
            // GraphReconstructionByNC
            gr = new GraphReconstructionByNC(g, iterations, anon, fullPath, baseName, extension);
        } else if (edgeSelection.compareToIgnoreCase(GraphReconstructionByCoreness.PARAM) == 0){
            // GraphReconstructionByCoreness
            gr = new GraphReconstructionByCoreness(g, iterations, anon, fullPath, baseName, extension);
        } else {
            logger.info("Invalid Reconstruction type!");
            return;
        }
        // apply perturbation
        gr.perturbation();

        logger.info("Perturbation process has finished!");
        logger.info("************************************************");
        
        et.stop();
        logger.info(String.format("Total running time: %s", et.getElapsedTime()));
        logger.info("************************************************");
    }
}
