/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.uoc.kison.RGO;

import interviews.graphs.Edge;
import interviews.graphs.Graph;
import interviews.graphs.KCore;

import java.io.File;
import java.util.Iterator;

import org.uoc.kison.exporters.GmlExporter;
import org.uoc.kison.objects.SimpleIntGraph;

/**
 *
 * @author jcasasr
 */
public class GraphReconstructionByCoreness extends GraphReconstruction {

    private Graph graph;
    private Graph graphK;
    private KCore kCore;
    
    public static final String PARAM = "DE";
    public static final String NAME = "Coreness";
    public static final int NUM = 1;

    public GraphReconstructionByCoreness(SimpleIntGraph g, int iterations, int anon, String fullPath, String baseName, String extension) {
    	super(g, iterations, anon, fullPath, baseName, extension);
    	 
        // Convert 'SimpleIntGraph' to 'Graph'
        this.convertSimpleIntGraphToGraph();
        
        logger.debug(String.format("INFO"));
        logger.debug(String.format("graph  Nodes=%s, edges=%s", graph.V, graph.E()));
        
        // initialize 'KCore' as unweighted graph
        this.kCore = new KCore(this.graph);
        this.kCore.computeUnweighted();
        
        logger.info(String.format("GraphReconstructionByCoreness: Using method '%s'", NAME));
    }
    
    /**
     * convert 'g' to 'graph'
     */
    private void convertSimpleIntGraphToGraph() {
        // create an empty graph
        this.graph = new Graph(this.g.getNumNodes());
        
        // add edges
        for(int i=0; i<g.getNumNodes(); i++) {
            for(int j: g.getEdges(i)) {
                /**
                 * TODO: aquesta part dona problemes al carregar "LiveJournal" (ArrayIndexOfBoundExcepctions)
                 * Intenta insertar una aresta amb numNode > max
                 */
                if(j < g.getNumNodes()) {
                    graph.addEdge(i, j);
                }
            }
        }
    }
    
   @Override
   public void perturbation() {
       
       GmlExporter gmlExporter = new GmlExporter();
       
       logger.info("Starting graph perturbation...");
       
       for(int i=1; i<=this.iterations; i++) {
           // gk = g
           this.graphK = utilsGraph.copyGraph(graph);
           
           // perturbe 'gk'  anonNumSet.length times
           for(int a=0; a<this.anonNumSet.length; a++) {

                // perturbe 'gk' using anonNumSet values
                createPerturbedGraph(this.anonNumSet[a]);

                // export result to GML
                String outputFileName = this.getFullPath(i, a);
                logger.info(String.format("Graph properties          : %d nodes, %d edges", graphK.V, graphK.E()));
                logger.info(String.format("Saving anonymized graph to: %s", outputFileName));
                gmlExporter.exportToFile(graphK, outputFileName);
           }
       }
       
       logger.info("Graph perturbation done!");
   }
   
   @Override
   protected void createPerturbedGraph(int anonNum) {
       
       int num = this.utilsGraph.edgeDifference(graph, graphK);
       
       logger.info(String.format("Creating anonNum=%s (starting=%s)", anonNum, num));

       while(num < anonNum) {
           // apply one edge modification
           int n = oneEdgeModification();
           
           num = num + n;
       }
       
       graphK = graph;
   }
    
    @Override
    protected int oneEdgeModification() {
        
        // delete + add one edge
        if(this.oneEdgeDeletion() && this.oneEdgeAddition()) {
            return NUM;
        } else {
            return 0;
        }
    }
    
    @Override
    protected String getFullPath(int i, int a) {
        File file = new File(fullPath + PARAM + "/");
        file.mkdirs();
    	return fullPath + PARAM + "/"+ baseName + "-" + i + "-" + a + "." + extension;
    }
    
    /**
     * 
     * @return 
     */ 
    private boolean oneEdgeDeletion() {

        boolean done = false;
        while(!done) {
            int numE = (int) Math.round(Math.random() * (this.graph.E() - 1));
            Iterator<Edge> ie = this.graph.edges().iterator();
            
            while(numE > 0) {
                ie.next();                
                numE --;
            }
            Edge e = ie.next();
            
            // test coreness
            if(this.kCore.removeEdge(e)) {
                // edge removed
                logger.debug(String.format("Deleting edge (%s,%s)...", e.v, e.w));

                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @return 
     */
    private boolean oneEdgeAddition() {
        
        boolean done = false;
        while(!done) {
            int v = (int) Math.round(Math.random() * (this.graph.V - 1));
            int w = (int) Math.round(Math.random() * (this.graph.V - 1));
            
            // no self-loops
            if(v != w) {
                Edge e = new Edge(v, w);

                // e=(vi, vj) exist?
                if(!this.graph.contains(e)) {

                    // test coreness
                    if(this.kCore.addEdge(e)) {
                        logger.debug(String.format("Adding edge (%s,%s)...", e.v, e.w));

                        return true;
                    }
                }
            }
        }
        
        return false;
    }

	@Override
	void doPreProcess() {		
	}

	@Override
	void doPostProcess() {
	}
}
