/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.uoc.kison.RGO;

import interviews.graphs.Edge;
import java.io.File;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.uoc.kison.RGO.utils.UtilsGraph;
import org.uoc.kison.exporters.GmlExporter;
import org.uoc.kison.objects.SimpleIntGraph;

abstract class GraphReconstruction {

    protected static final Logger logger = Logger.getLogger(GraphReconstruction.class);
    protected UtilsGraph utilsGraph;
    protected SimpleIntGraph g; // original graph
    protected SimpleIntGraph gk; // anonymized graph
    protected int iterations;
    protected int anon;
    protected int[] anonNumSet;
    protected String fullPath;
    protected String baseName;
    protected String extension;

    /**
     *
     * @param g: original graph
     */
    public GraphReconstruction(SimpleIntGraph g, int iterations, int anon, String fullPath, String baseName, String extension) {
        this.utilsGraph = new UtilsGraph();
        this.iterations = iterations;
        this.anon = anon;
        this.fullPath = fullPath;
        this.baseName = baseName;
        this.extension = extension;

        if (g != null) {
            this.g = g;

            // create
            this.anonNumSet = new int[anon + 1];
            // si no aplica el doble de modificacions!
            int numEdges = Math.round(g.getNumEdges() / 2);
            for (int i = 0; i <= anon; i++) {
                this.anonNumSet[i] = Math.round(i * numEdges / 100);
            }

            logger.debug(String.format("INFO"));
            logger.debug(String.format("G  Nodes=%s, edges=%s", g.getNumNodes(), g.getNumEdges()));
        }
    }

    /**
     *
     */
    public void perturbation() {

        GmlExporter gmlExporter = new GmlExporter();

        logger.info("Starting graph perturbation...");

        for (int i = 1; i <= this.iterations; i++) {
            // gk = g
            this.gk = utilsGraph.copyGraph(g);

            // perturbe 'gk'  anonNumSet.length times
            for (int a = 0; a < this.anonNumSet.length; a++) {

                // check whether the anonymized graph exists
                String outputFileName = this.getFullPath(i, a);

                File f = new File(outputFileName);
                if (!f.exists()) {
                    // perturbe 'gk' using anonNumSet values
                    createPerturbedGraph(this.anonNumSet[a]);

                    logger.info(String.format("Graph properties          : %d nodes, %d edges", gk.getNumNodes(), gk.getNumEdges()));
                    logger.info(String.format("Saving anonymized graph to: %s", outputFileName));
                    // export result to GML
                    gmlExporter.exportToFile(gk, outputFileName);
                } else {
                    // it exists! Do not replace it!
                    logger.info(String.format("Anonymized graph %s already exists!", outputFileName));
                }

            }
        }

        logger.info("Graph perturbation done!");
    }

    /**
     * Create perturbed graph
     */
    protected void createPerturbedGraph(int anonNum) {

        // pre-process
        this.doPreProcess();

        int num = this.utilsGraph.edgeDifference(g, gk);

        logger.info(String.format("Creating anonNum=%s (starting=%s)", anonNum, num));

        while (num < anonNum) {
            // apply one edge modification
            int n = oneEdgeModification();

            num = num + n;
        }

        // post-process
        this.doPostProcess();
    }

    protected int getRandomEdgeNumber() {
        return (int) Math.round(Math.random() * (this.gk.getNumEdges() - 1));
    }

    protected int getRandomVertexNumber() {
        return (int) Math.round(Math.random() * (this.gk.getNumNodes() - 1));
    }

    protected void deleteEdge(int v, int w) {
        if (this.gk.getEdges(v).contains(w)) {
            this.gk.deleteEdge(v, w);
            this.gk.deleteEdge(w, v);
        } else {
            logger.error(String.format("Error, trying to delete a non-existing edge (%s,%s)", v, w));
            System.exit(0);
        }
    }

    protected void addEdge(int v, int w) {
        if (!this.gk.getEdges(v).contains(w)) {
            this.gk.addEdge(v, w);
            this.gk.addEdge(w, v);
        } else {
            logger.error(String.format("Error, trying to add an existing edge (%s,%s)", v, w));
            System.exit(0);
        }
    }

    protected Edge getRandomEdge() {
        // get a random number between 0..m-1
        int e = this.getRandomEdgeNumber();

        boolean done = false;
        int i = 0;
        while (!done) {
            ArrayList<Integer> ar = this.gk.getEdges(i);
            if (e < ar.size()) {
                // edge found!
                int v = i;
                int w = ar.get(e);

                return new Edge(v, w);
            } else {
                e = e - ar.size();
            }
            i++;
        }

        return null;
    }

    abstract int oneEdgeModification();

    abstract String getFullPath(int i, int a);

    abstract void doPreProcess();

    abstract void doPostProcess();
}
