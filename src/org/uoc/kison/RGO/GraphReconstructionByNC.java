/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.uoc.kison.RGO;

import java.io.File;
import java.util.ArrayList;

import org.uoc.kison.RGO.metrics.EdgeNeighbourhoodCentrality;
import org.uoc.kison.RGO.metrics.FakeEdge;
import org.uoc.kison.objects.SimpleIntGraph;

/**
 *
 * @author jcasasr
 */
public class GraphReconstructionByNC extends GraphReconstruction {

	public static final String PARAM = "NC";
	public static final String NAME = "Neighbourhood Centrality";
	public static final int NUM = 1;

	private ArrayList<Double> pEdges;
	private ArrayList<FakeEdge> fakeEdges;

	public GraphReconstructionByNC(SimpleIntGraph g, int iterations, int anon, String fullPath, String baseName, String extension) {
		super(g, iterations, anon, fullPath, baseName, extension);

		logger.info(String.format("GraphReconstructionByNC: Using method '%s'", NAME));
	}

	@Override
	int oneEdgeModification() {

		/*******************
		 ** DELETE 1 EDGE  **
		 ********************/	  
		int num = getSampleWithProbabilities(pEdges);
		// select edge
		int[] e = getEdge(num);
		// verify
		if(!gk.getEdges(e[0]).contains(e[1])) {
			logger.error("edgeNC:: Trying to remove an edge, but it does not exist!");
			return -1;
		}
		// delete edge
		this.deleteEdge(e[0], e[1]);
		// remove used item
		pEdges.remove(num);

		/***************
		 *  ADD 1 EDGE  *
		 ****************/
		boolean found = false;
		int source = -1;
		int target = -1;
		
		ArrayList<Double> probFakeEdges = new ArrayList<Double>(fakeEdges.size());
		for(int i=0; i<fakeEdges.size(); i++) probFakeEdges.add(fakeEdges.get(i).getProbability());
		while(!found) {
			num = getSampleWithProbabilities(probFakeEdges);
			// select edge
			source = fakeEdges.get(num).getSource();
			target = fakeEdges.get(num).getTarget();
			// delete edge
			if(gk.getEdges(source).contains(target)) {
				logger.warn("edgeNC:: Trying to add a fake edge, but a real one exists!");
			} else {
				found = true;
			}
		}
		this.addEdge(source, target);
		
		// remove used item
		fakeEdges.remove(num);

		return NUM;
	}

	@Override
	String getFullPath(int i, int a) {
		File file = new File(fullPath + PARAM + "/");
		file.mkdirs();
		return fullPath + PARAM + "/"+ baseName + "-" + i + "-" + a + "." + extension;
	}

	@Override
	void doPreProcess() {
		EdgeNeighbourhoodCentrality EdgeNC = new EdgeNeighbourhoodCentrality();

		// compute Probabilities
		ArrayList<Double> ncEdges = EdgeNC.computeEdgeNeighbourhoodCentrality(gk);
		pEdges = EdgeNC.getProbabilityDistribution(ncEdges, EdgeNeighbourhoodCentrality.OneOverX2);

		//create fake edges set
		fakeEdges = EdgeNC.computeFakeEdgesSet(gk);		
	}

	@Override
	void doPostProcess() {		
	}

	private int getSampleWithProbabilities(ArrayList<Double> probability){
		double sum = 0d;
		for (int i=0;i<probability.size(); i++) sum += probability.get(i);
		double r = Math.random() * sum;
		
		double countWeight = 0.0;
		for (int i=0; i<probability.size(); i++) {
			countWeight += probability.get(i);
			if (countWeight >= r) return i;
		}
		return -1; // should never happend!
	}

	private int[] getEdge(int edgeNum){
		int[] result = new int[2];
		int numNodes = gk.getNumNodes();
		int count = 0;
		for(int i=0; i<numNodes; i++){
			int adjSize = gk.getEdges(i).size();
			if (count + adjSize > edgeNum){
				int pos = edgeNum - count;
				result[0] = i;
				result[1] = gk.getEdges(i).get(pos);
				return result;
			}else{
				count += adjSize;
			}
		}
		return null; // should never happend
	}

}
