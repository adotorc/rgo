/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 * 
 * This file is part of RGO. 
 * 
 * RGO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RGO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with RGO.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.uoc.kison;

import interviews.graphs.Graph;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.uoc.kison.RGO.GraphReconstructionByCoreness;
import org.uoc.kison.RGO.GraphReconstructionByNC;
import org.uoc.kison.RGO.GraphReconstructionByRP;
import org.uoc.kison.RGO.GraphReconstructionByRS;
import org.uoc.kison.RGO.Process;
import org.uoc.kison.objects.SimpleIntGraph;
import org.uoc.kison.parsers.GmlParser;
import org.uoc.kison.parsers.TxtParser;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);
    private static final String version = "1.0";

    public static void main(String[] args) {
        PropertyConfigurator.configure("log4j.properties");

        String inputFileName = null;
        String extension = null;
        String fullPath = null;
        String baseName = null;
        int iterations = 1;
        int anon = 1;
        String edgeSelection = null;

        if (args.length >= 4) {
            inputFileName = args[0];
            iterations = Integer.parseInt(args[1]);
            anon = Integer.parseInt(args[2]);
            edgeSelection = args[3];
            
            fullPath = FilenameUtils.getFullPath(inputFileName);
            baseName = FilenameUtils.getBaseName(inputFileName);
            extension = FilenameUtils.getExtension(inputFileName);
            
            logger.info(String.format("Random Graph Obfuscation (RGO)"));
            logger.info(String.format("Version %s", version));
            logger.info(String.format("Input filname   : %s", inputFileName));
            logger.info(String.format("Iterations      : %d", iterations));
            logger.info(String.format("Anon            : %s", anon));
            logger.info(String.format("Edge selection  : %s", edgeSelection));
            logger.info("");
            logger.info("---------------------------------------------------------------");

        } else {
            System.out.println("Random Graph Obfuscation (RGO)");
            System.out.println("Usage: java RGO <input filename> <iterations> <anon> <edge selection>");
            System.out.println("   - <iterations>     : Number of independent executions (1 is the default) (integer value)");
            System.out.println("   - <anon>           : Maximum anonymization percentage (from 0% by 1%) (integer value)");
            System.out.println("   - <edge selection> : '" + GraphReconstructionByCoreness.PARAM + "' for "+ GraphReconstructionByCoreness.NAME);
            System.out.println("                        '" + GraphReconstructionByRP.PARAM + "' for "+ GraphReconstructionByRP.NAME);
            System.out.println("                        '" + GraphReconstructionByRS.PARAM + "' for "+ GraphReconstructionByRS.NAME);
            System.out.println("                        '" + GraphReconstructionByNC.PARAM + "' for "+ GraphReconstructionByNC.NAME);
            System.exit(-1);
        }

        // import graph
        SimpleIntGraph graph = null;
        Graph igraph = null;

        if (extension.compareToIgnoreCase("GML") == 0) {
            GmlParser gmlParser = new GmlParser();
            graph = gmlParser.parseFileToSimpleIntGraph(inputFileName);

        } else if (extension.compareToIgnoreCase("TXT") == 0) {
            TxtParser txtParser = new TxtParser();
            graph = txtParser.parseFileToSimpleIntGraph(inputFileName);
            
        } else {
            logger.error(String.format("Unknown filetype (extension %s)!", extension));
            System.exit(0);
        }

        // apply RGO
        Process process = new Process(fullPath, baseName, extension);
        process.execute(graph, iterations, anon, edgeSelection);
    }
}
